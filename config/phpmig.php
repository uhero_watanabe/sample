<?php

use \Pimple;
use \Phpmig\Adapter;

$container = new Pimple();

// replace this with a better Phpmig\Adapter\AdapterInterface
$container['phpmig.adapter'] = 
    new Adapter\File\Flat(__DIR__ . '/migrations/.migrations.log');
$container['phpmig.migrations_path'] = __DIR__ . '/migrations';

$env = (isset($_SERVER["APP_ENV"]) && $_SERVER["APP_ENV"] === "prod") ? "prod" : "dev";
$container["db"]  = call_user_func(function () use ($env) {
    $jsonpath = __DIR__ . "/database-{$env}.json";
    $json     = file_get_contents($jsonpath);
    $settings = json_decode($json);
    $host     = $settings->host;
    $dbname   = $settings->database;
    return new PDO("mysql:dbname={$dbname};host={$host}", 
        $settings->user, $settings->password);
});
$container["env"] = $env;

return $container;
