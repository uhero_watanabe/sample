<?php

use Phpmig\Migration\Migration;

class CreateMembers extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();
        $sql = <<<EOS
create table if not exists members (
    `id`    int         not null auto_increment,
    `name`  varchar(16) not null,
    primary key (`id`)
);
insert into members (`id`, `name`) values
(1, 'Taro'),
(2, 'Jiro'),
(3, 'Hanako'),
(4, 'Anna');
EOS;
        $container["db"]->query($sql);
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();
        $container["db"]->query("drop table if exists members");
    }
}
