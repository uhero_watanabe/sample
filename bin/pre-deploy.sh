approot="${HOME}/workspace"
while getopts "d:" OPT; do
    case ${OPT} in
        "d" ) approot=${OPTARG} ;;
    esac
done

while read FROM DEST BRANCH; do
    # master ブランチ以外の push を受け取った場合は何もせずに終了します
    if [ "${BRANCH}" != "refs/heads/master" ]; then
        break
    fi
    
    cd ${approot}
    git --git-dir=.git diff --name-status origin/${FROM} origin/${DEST} | while read MODE NAME; do
        if [[ ${NAME} =~ ^config/migrations/([0-9]+)_.+\.php ]]; then
            if [ ${MODE} = "D" ]; then
                vendor/bin/phpmig down ${BASH_REMATCH[1]}
            fi
        fi
    done
done
