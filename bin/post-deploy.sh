approot="${HOME}/workspace"
while getopts "d:" OPT; do
    case ${OPT} in
        "d" ) approot=${OPTARG} ;;
    esac
done

while read FROM DEST BRANCH; do
    # master ブランチ以外の push を受け取った場合は何もせずに終了します
    if [ "${BRANCH}" != "refs/heads/master" ]; then
        break
    fi
    
    # 初回デプロイ時に phpmig をインストールします
    cd ${approot}
    if [ ! -d vendor ]; then
        php composer.phar install
    fi

    if [ "${FROM}" = "0000000000000000000000000000000000000000" ]; then
        find config/migrations/*.php | while read NAME; do
            if [[ ${NAME} =~ ^config/migrations/([0-9]+)_.+\.php ]]; then
                vendor/bin/phpmig up ${BASH_REMATCH[1]}
            fi
        done
    else
        git --git-dir=.git diff --name-status ${FROM} ${DEST} | while read MODE NAME; do
            if [[ ${NAME} =~ ^config/migrations/([0-9]+)_.+\.php ]]; then
                if [ ${MODE} = "A" ]; then
                    vendor/bin/phpmig up ${BASH_REMATCH[1]}
                fi
            fi
        done
    fi
done
