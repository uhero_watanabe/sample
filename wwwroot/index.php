<?php
$env      = isset($_SERVER["APP_ENV"]) ? $_SERVER["APP_ENV"] : "dev";
$jsonpath = dirname(__DIR__) . "/config/database-{$env}.json";
$json     = file_get_contents($jsonpath);
$settings = json_decode($json);
$host     = $settings->host;
$dbname   = $settings->database;
$pdo      = new PDO("mysql:dbname={$dbname};host={$host}", 
    $settings->user, $settings->password);
$stmt     = $pdo->prepare("select id, name from members");
$stmt->execute();
$members = $stmt->fetchAll();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Sample Website</title>
</head>
<body>
    <h1>Sample Website</h1>
    <p>Here: <?= __FILE__ ?></p>
    <p>Now: <?= date("Y-m-d H:i:s"); ?></p>
    <h2>Members</h2>
    <ul>
<?php foreach ($members as $member): ?>
        <li>ID=<?= $member["id"] ?>, NAME=<?= $member["name"] ?></li>
<?php endforeach; ?>
    </ul>
</body>
</html>
